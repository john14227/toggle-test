import * as React from 'react';
import './index.css';

interface IToggleProps{
    checked: boolean;
    onChange: (checked: boolean) => void;
    disabled?: boolean;
}

export class Toggle extends React.Component <IToggleProps>{
    constructor(props: IToggleProps) {
        super(props);
    }

    public render() {
        const checked = this.props.checked
        const onChange = this.props.onChange.bind(this,checked);
        return (
            <div>
                <label className="switch">
                    <input type="checkbox" 
                    checked={this.props.checked}
                    onChange={onChange}
                    disabled={this.props.disabled}
                    />
                    <span className="slider round" 
                    />
                </label>
            </div>
        )
    }
}