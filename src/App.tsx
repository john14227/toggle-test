import * as React from 'react';
import {Toggle} from './Switch/index'


interface IAppState{
  checked:boolean
  disabled?:boolean
}

class App extends React.Component<any,IAppState> {
  
  constructor(props:any){
    super(props);
    this.state = {
      checked:false,
      disabled:false
    }
  }

  private onChange = (checked:boolean) => {
    this.setState({
      checked:!this.state.checked
    })
  }

  public render() {
    return (
      <div className="App">
        <Toggle 
        checked={this.state.checked}
        onChange={this.onChange}
        />
      </div>
    );
  }
}

export default App;
